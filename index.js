let apikey = "472148b4449400ccb0b96edcdbe780bc";
let arrayDirection = ['nord','nord-est','est','sud-est','sud','sud-ouest','ouest','nord-ouest']
function cityInfo() {
    let input_value = document.getElementById("city-name").value

    const api_city = `https://api.openweathermap.org/data/2.5/weather?q=${input_value}&units=metric&appid=${apikey}`;
    fetch(api_city).then((response) => 
        response.json().then((data) => {
            if(data.name !==undefined){
                document.getElementById('bordered1').className = "border-top"
                document.getElementById('bordered2').className = "border-top"
                document.getElementById('bordered3').className = "border-top"
                document.getElementById('temp').className = "fas fa-temperature-low m-3"
                document.getElementById('water').className = "fas fa-tint m-3"
                document.getElementById('wind').className = "fas fa-wind m-3"
                document.getElementById('city_name').className="m-3"
                document.getElementById('city_name').style="padding-left: 17%"
                document.getElementById('city_name').innerHTML = data.name;
                document.getElementById('weathericon').src = `http://openweathermap.org/img/wn/${data.weather[0].icon}@2x.png`;
                document.getElementById('tempmini').innerHTML = "La temperature minimum est de "+Math.round(data.main.temp_min*100)/100+" ° Celsius";
                document.getElementById('tempmaxi').innerHTML = "La temperature maximum est de "+Math.round(data.main.temp_max*100)/100+" ° Celsius";
                document.getElementById('tempfeels').innerHTML = "La temperature ressentie est de "+Math.round(data.main.feels_like*100)/100+" ° Celsius";
                document.getElementById('windspeed').innerHTML = "La vitesses du vent est de "+Math.round((data.wind.speed)*3.6*100)/100+" km/h";
                document.getElementById('winddirection').innerHTML = "La direction du vent est "+arrayDirection[Math.round(data.wind.deg/45)];
                document.getElementById('humidity').innerHTML = "Le taux d'humidité est de "+Math.round(data.main.humidity*100)/100+" %";
            }else {
                document.getElementById('bordered1').className = ""
                document.getElementById('bordered2').className = ""
                document.getElementById('bordered3').className = ""
                document.getElementById('temp').className = ""
                document.getElementById('water').className = ""
                document.getElementById('wind').className = ""
                document.getElementById('city_name').innerHTML = "NOM INCORRECT"
                document.getElementById('city_name').className="text-center m-3"
                document.getElementById('city_name').style=""
                document.getElementById('weathericon').src = ``;
                document.getElementById('tempmini').innerHTML = "";
                document.getElementById('tempmaxi').innerHTML = "";
                document.getElementById('tempfeels').innerHTML = "";
                document.getElementById('windspeed').innerHTML = "";
                document.getElementById('winddirection').innerHTML = "";
                document.getElementById('humidity').innerHTML = "";
            }
        }))
    .catch((error) => {
        console.log(error);
    })
}







